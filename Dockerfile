#docker run -it --rm \
#   -v /etc/localtime:/etc/localtime:ro \
#   -v /tmp/.X11-unix:/tmp/.X11-unix \
#   -v ${HOME}/.config/Postman/:/root/.config/Postman \
#   -e DISPLAY=unix$DISPLAY \
#   jrigney/postman:5.3.2 $@
FROM debian:sid

WORKDIR /usr
RUN apt-get update && \
    apt-get install -y libnss3 libasound2 \
    libx11-xcb-dev \
    libgconf-2-4 libxtst6 \
    ca-certificates wget libxss1 libgtk2.0-0 && \
    update-ca-certificates

RUN wget -O /tmp/postman.tar.gz https://dl.pstmn.io/download/latest/linux64 && \
    tar zxf /tmp/postman.tar.gz -C / && \
    rm /tmp/postman.tar.gz

CMD ["/Postman/Postman"]
